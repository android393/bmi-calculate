package com.example.bmicalculate

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.example.bmicalculate.databinding.ActivityMainBinding
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener { calculateBMI() }

    }
    @SuppressLint("SetTextI18n")
    private fun calculateBMI() {
        val costWeight = binding.weightEditText.text.toString().toDoubleOrNull()
        val costHeight = binding.heightEditText.text.toString().toDoubleOrNull()

        if (costHeight == 0.0 || costHeight == null || costWeight == 0.0 || costWeight == null) {
            binding.bmiScore.text = getString(R.string.bmi_score_text, "0.0")
            return
        }
        val numBMI = costWeight / ((costHeight / 100) * (costHeight / 100))
        val df = DecimalFormat("#.#")
        val formatBMI = df.format(numBMI)
        binding.bmiScore.text = getString(R.string.bmi_score_text, formatBMI)

        if (formatBMI.toDouble() < 18.5) {
            binding.bmiResult.text = "UNDERWEIGHT"
        } else if (formatBMI.toDouble() < 24.9) {
            binding.bmiResult.text = "NORMAL"
        } else if (formatBMI.toDouble() < 29.9) {
            binding.bmiResult.text = "OVERWEIGHT"
        } else {
            binding.bmiResult.text = "OBESE"
        }

    }
}